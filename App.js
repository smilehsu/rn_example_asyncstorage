import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
// import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

import Home from "./src/screens/Home"
import Login from "./src/screens/Login"


const Stack = createNativeStackNavigator();


const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName='Login'
        screenOptions={
          {
            headerTitleAlign: "center",
            headerStyle: {
              backgroundColor: "#0080ff"
            },
            headerTintColor: "#fff",
            headerTitleStyle: {
              fontSize: 25,
              fontWeight: "bold"
            }
          }
        }>
        {/* 內頁 */}
        <Stack.Screen
          name="Login"
          component={Login}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen
          name="Home"
          component={Home}
        />
      </Stack.Navigator>
    </NavigationContainer >
  )
}

export default App

const styles = StyleSheet.create({})
