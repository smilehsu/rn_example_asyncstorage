import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, TextInput, Alert } from 'react-native'
import GlobalStyle from "../utils/GlobalStyle"
import AsyncStorage from '@react-native-async-storage/async-storage'
import CustomButton from "../utils/CustomButton"

const Home = ({ navigation }) => {

    const [name, setName] = useState("")
    const [age, setAge] = useState("")


    // 重新渲染畫面
    useEffect(() => {
        getData()
    }, [])

    const getData = () => {
        try {
            AsyncStorage.getItem("UserData")
                .then(value => {
                    if (value != null) {
                        let user = JSON.parse(value)
                        setName(user.Name)
                        setAge(user.Age)
                    }
                }

                )
        } catch (error) {
            console.log("錯誤訊息", error)

        }
    }

    //修改
    const updateData = async () => {
        if (name.length == 0) {
            Alert.alert("Warring!", "請輸入ID!!!")
        } else {
            try {
                let user = {
                    Name: name
                }
                await AsyncStorage.mergeItem("UserData", JSON.stringify(user))
                Alert.alert("Success", "Your data has been updated!")
            } catch (error) {
                console.log("錯誤訊息: ", error)
            }
        }
    }

    //刪除
    const removeData = async () => {
        if (name.length == 0) {
            Alert.alert("Warring!", "請輸入ID!!!")
        } else {
            try {
                //可用await AsyncStorage.clear()
                await AsyncStorage.removeItem("UserName")
                navigation.navigate("Login")
                Alert.alert("Success", "Your data has been updated!")
            } catch (error) {
                console.log("錯誤訊息: ", error)
            }
        }
    }


    return (
        <View style={styles.body}>
            <Text style={[GlobalStyle.CustomFontHW, styles.text]}>welcome {name} !!!</Text>
            <Text style={[GlobalStyle.CustomFontHW, styles.text]}>Your age is {age} !!!</Text>
            <TextInput
                style={styles.TextInput}
                placeholder='Enter your name'
                onChangeText={(value) => setName(value)}
                value={name}
            />
            <CustomButton
                title="Update"
                color="#ff7f00"
                onPressFunction={updateData}
            />
            <CustomButton
                title="Delete"
                color="#f40100"
                onPressFunction={removeData}
            />
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: "center",

    },
    text: {
        fontSize: 30,
        color: "#0f0",
        fontWeight: "bold",
    },
    TextInput: {
        width: 300,
        borderWidth: 1,
        borderColor: "#555",
        borderRadius: 15,
        backgroundColor: "#fff",
        textAlign: "center",
        fontSize: 20,
        marginTop: 40,
        marginBottom: 10,
    },


})
