import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Image, TextInput, Alert } from 'react-native'
import CustomButton from "../utils/CustomButton"

import AsyncStorage from '@react-native-async-storage/async-storage'

const Login = ({ navigation }) => {
    const [name, setName] = useState('')
    const [age, setAge] = useState("")

    // 重新渲染畫面
    useEffect(() => {
        getData()
    }, [])
    const getData = () => {
        try {
            AsyncStorage.getItem("UserData")
                .then(value => {
                    if (value != null) {
                        navigation.navigate("Home")
                    }
                }

                )
        } catch (error) {
            console.log("錯誤訊息", error)

        }
    }

    const setData = async () => {
        if (name.length == 0 || age.length == 0) {
            Alert.alert("Warring!", "輸入欄位不能空白!!!")
        } else {
            try {
                let user = {
                    Name: name,
                    Age: age
                }
                await AsyncStorage.setItem("UserData", JSON.stringify(user))
                // console.log("UserName: ", name)
                //回到 Home頁
                navigation.navigate("Home")
            } catch (error) {
                console.log("錯誤訊息: ", error)
            }
        }
    }

    return (
        <View style={styles.body}>
            <Image
                style={styles.logo}
                source={require("../../assets/user-login-305.png")}
            />
            <Text style={styles.text}>Async Storage 範例</Text>
            <TextInput
                placeholder='請輸入你的ID'
                style={styles.textInput}
                onChangeText={(text) => setName(text)}
            />
            <TextInput
                style={styles.textInput}
                placeholder='Enter your age'
                onChangeText={(value) => setAge(value)}
                value={age}
            />
            <CustomButton
                title="Login"
                color="#1eb900"
                onPressFunction={setData}
            />
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    body: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#0080ff"
    },
    logo: {
        width: 100,
        height: 100,
        margin: 20
    },
    text: {
        fontSize: 30,
        color: "#fff",
        fontWeight: "bold",
        marginBottom: 40,
    },
    textInput: {
        width: 300,
        borderWidth: 1,
        borderColor: "#555",
        borderRadius: 15,
        backgroundColor: "#fff",
        textAlign: "center",
        fontSize: 20,

        marginBottom: 10,

    }
})
